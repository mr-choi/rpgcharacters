# rpgcharacters

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

This module is the first assignment of the backend part of the full stack Java developer course at Noroff academy. The goals of this assignment is to program in an object oriented manner, to make use of unit tests and to make use of CI to run the tests.

## Table of Contents

- [rpgcharacters](#rpgcharacters)
  - [Table of Contents](#table-of-contents)
  - [Install](#install)
  - [Usage](#usage)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)
  - [License](#license)

## Install
Fork and clone this repository to get the contents.
You need to use maven to run all the tests with the command `mvn test`.

## Usage
The code is a basis to implement role playing game (RPG) characters.
There are 4 types of characters: mage, ranger, rogue, and warrior.
All types of characters have three primary attributes: strength, dexterity, and intelligence.
One of the attributes is considered main and improves the damage per second (DPS) a character can perform.

In general, a character starts at level 1.
When a character levels up, the base primary attributes of the character will increase.

Any character can equip items: either a weapon or a piece of armor.
Weapons are needed to deal damage, while armor improves the primary attributes of the characters.
To equip items, the character needs to have a minimum level for that item.
Moreover, the type of weapon or armor must fit the character type.

## Maintainers

[@mr-choi](https://gitlab.com/mr-choi)

## Contributing
This is a personal project and the purpose of this code is to fullfill an assignment I had to do.
Therefore, I do not expect contribution.
Feel free, however, to fork this project if you want to make use of the code


Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Kevin Choi

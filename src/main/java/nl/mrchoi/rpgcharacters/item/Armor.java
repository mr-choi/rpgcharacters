package nl.mrchoi.rpgcharacters.item;

import nl.mrchoi.rpgcharacters.util.ArmorType;
import nl.mrchoi.rpgcharacters.util.PrimaryAttributes;
import nl.mrchoi.rpgcharacters.util.Slot;

/**
 * Armor that characters can equip to improve their primary attributes
 */
public class Armor extends Item {

    private final ArmorType type;
    private final PrimaryAttributes attributes;
    /**
     * Initialize new armor.
     *
     * @param name         The name of the armor.
     * @param minimumLevel The minimum level needed to equip the armor.
     * @param slot         The slot where the armor will be equipped.
     * @param type         The type of armor.
     * @param attributes   The primary attributes gain when equipping the armor.
     */
    public Armor(String name, int minimumLevel, Slot slot, ArmorType type, PrimaryAttributes attributes) {
        super(name, minimumLevel, slot);
        this.type = type;
        this.attributes = attributes;
    }

    /**
     * The type of armor.
     * @return The type of armor.
     */
    public ArmorType getType() {
        return type;
    }

    /**
     * The primary attributes gain when equipping the armor.
     * @return The primary attributes gain when equipping the armor.
     */
    public PrimaryAttributes getAttributes() {
        return attributes;
    }
}

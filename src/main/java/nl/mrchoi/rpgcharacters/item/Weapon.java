package nl.mrchoi.rpgcharacters.item;

import nl.mrchoi.rpgcharacters.util.Slot;
import nl.mrchoi.rpgcharacters.util.WeaponType;

/**
 * Weapons that characters can equip to deal damage.
 */
public class Weapon extends Item {

    private final WeaponType type;
    private final double damage;
    private final double attackSpeed;

    /**
     * Initialize new item.
     *
     * @param name         The name of the weapon.
     * @param minimumLevel The minimum level needed to equip the weapon.
     * @param type         The type of weapon.
     * @param damage       The amount of damage a single hit can deal.
     * @param attackSpeed  The number of attacks per second this weapon can do.
     */
    public Weapon(String name, int minimumLevel, WeaponType type, double damage, double attackSpeed) {
        super(name, minimumLevel, Slot.WEAPON);
        this.type = type;
        this.damage = damage;
        this.attackSpeed = attackSpeed;
    }

    /**
     * The type of weapon.
     * @return The type of weapon.
     */
    public WeaponType getType() {
        return type;
    }

    /**
     * The damage per second the weapon can do.
     * @return The damage per second the weapon can do.
     */
    public double getDPS() {
        return this.attackSpeed * this.damage;
    }
}

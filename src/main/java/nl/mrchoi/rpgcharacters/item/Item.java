package nl.mrchoi.rpgcharacters.item;

import nl.mrchoi.rpgcharacters.util.Slot;

/**
 * Item that can be equipped by characters.
 */
public abstract class Item {
    private final String name;
    private final int minimumLevel;
    private final Slot slot;

    /**
     * Initialize new item.
     * @param name The name of the item.
     * @param minimumLevel The minimum level needed to equip the item.
     * @param slot The slot where the item will be equipped.
     */
    public Item(String name, int minimumLevel, Slot slot) {
        this.name = name;
        this.minimumLevel = minimumLevel;
        this.slot = slot;
    }

    /**
     * The name of the item.
     * @return The name of the item.
     */
    public String getName() {
        return name;
    }

    /**
     * The minimum level required to equip the item.
     * @return The minimum level required to equip the item.
     */
    public int getMinimumLevel() {
        return minimumLevel;
    }

    /**
     * The slot where the item will be equipped.
     * @return The slot where the item will be equipped.
     */
    public Slot getSlot() {
        return slot;
    }
}

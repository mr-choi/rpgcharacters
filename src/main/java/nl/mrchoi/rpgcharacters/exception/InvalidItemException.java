package nl.mrchoi.rpgcharacters.exception;

import nl.mrchoi.rpgcharacters.character.Character;
import nl.mrchoi.rpgcharacters.item.Item;
import nl.mrchoi.rpgcharacters.util.ItemType;

/**
 * Exception that will be thrown when a character cannot equip the item.
 */
public abstract class InvalidItemException extends Exception {
    public static final String LEVEL_TOO_LOW_MESSAGE_TEMPLATE =
            "Required level to equip %s (%d) is higher than level of %s (%d).";
    public static final String EQUIPMENT_TYPE_NOT_ALLOWED_TEMPLATE = "%s cannot equip %s of type %s.";
    public InvalidItemException(Item item, Character character) {
        super(String.format(LEVEL_TOO_LOW_MESSAGE_TEMPLATE, item.getName(), item.getMinimumLevel(),
                character.getName(), character.getLevel()));
    }
    public InvalidItemException(Character character, ItemType itemType, String itemCategory) {
        super(String.format(EQUIPMENT_TYPE_NOT_ALLOWED_TEMPLATE, character.getName(), itemCategory,
                itemType.getTypeName()));
    }
}

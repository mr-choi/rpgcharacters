package nl.mrchoi.rpgcharacters.exception;

import nl.mrchoi.rpgcharacters.character.Character;
import nl.mrchoi.rpgcharacters.item.Weapon;
import nl.mrchoi.rpgcharacters.util.WeaponType;

/**
 * Exception that will be thrown when a character cannot equip the weapon.
 */
public class InvalidWeaponException extends InvalidItemException {
    public InvalidWeaponException(Weapon weapon, Character character) {
        super(weapon, character);
    }
    public InvalidWeaponException(Character character, WeaponType weaponType) {
        super(character, weaponType, "weapon");
    }
}

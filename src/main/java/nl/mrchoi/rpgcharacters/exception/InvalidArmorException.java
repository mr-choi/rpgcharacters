package nl.mrchoi.rpgcharacters.exception;

import nl.mrchoi.rpgcharacters.character.Character;
import nl.mrchoi.rpgcharacters.item.Armor;
import nl.mrchoi.rpgcharacters.util.ArmorType;

/**
 * Exception that will be thrown in case the character cannot equip the armor.
 */
public class InvalidArmorException extends InvalidItemException {
    public InvalidArmorException(Armor armor, Character character) {
        super(armor, character);
    }
    public InvalidArmorException(Character character, ArmorType armorType) {
        super(character, armorType, "armor");
    }
}

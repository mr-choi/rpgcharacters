package nl.mrchoi.rpgcharacters.character;

import nl.mrchoi.rpgcharacters.exception.InvalidArmorException;
import nl.mrchoi.rpgcharacters.exception.InvalidWeaponException;
import nl.mrchoi.rpgcharacters.util.PrimaryAttributes;
import nl.mrchoi.rpgcharacters.util.Slot;
import nl.mrchoi.rpgcharacters.item.Armor;
import nl.mrchoi.rpgcharacters.item.Item;
import nl.mrchoi.rpgcharacters.item.Weapon;

import java.util.HashMap;

/**
 * Base class for all role playing game characters.
 */
public abstract class Character {
    private final String name;
    private int level;
    private PrimaryAttributes base;
    private final PrimaryAttributes gain;
    private HashMap<Slot, Item> equipment;

    /**
     * Initialize new character.
     * @param name The name of the character.
     * @param level The level the character start with.
     * @param start The attributes of the character at level 1.
     * @param gain The amount of attributes it gains every time it levels up.
     */
    public Character(String name, int level, PrimaryAttributes start, PrimaryAttributes gain) {
        this.name = name;
        this.level = level;
        this.gain = gain;
        this.base = start.add(gain.multiply(level-1));
        this.equipment = new HashMap<>();
    }

    /**
     * Increase the level of the character by 1.
     */
    public void levelUp() {
        this.level++;
        this.base = this.base.add(this.gain);
    }

    /**
     * Equip a weapon for the current character. If it already has a weapon equipped, it will be replaced.
     * @param weapon The weapon to be equipped.
     */
    public void equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if (weapon.getMinimumLevel() > this.level) {
            throw new InvalidWeaponException(weapon, this);
        }
        equipment.put(Slot.WEAPON, weapon);
    }

    /**
     * Equip armor for the current character. If it already has armor equipped at a specific slot, it will be replaced.
     * @param armor The armor to be equipped.
     */
    public void equipArmor(Armor armor) throws InvalidArmorException {
        if (armor.getMinimumLevel() > this.level) {
            throw new InvalidArmorException(armor, this);
        }
        equipment.put(armor.getSlot(), armor);
    }

    /**
     * Unequip an item for the current character.
     * @param slot The slot for which the item will be unequipped.
     */
    public void unequip(Slot slot) {
        equipment.remove(slot);
    }

    /**
     * Get the total primary attributes of the character.
     * @return The total primary attributes of the character.
     */
    public PrimaryAttributes getTotalPrimaryAttributes() {
        PrimaryAttributes total = base;
        for (Item item : equipment.values()) {
            if (!(item instanceof Armor)) continue;
            total = total.add(((Armor) item).getAttributes());
        }
        return total;
    }

    /**
     * Get the name of the character.
     * @return The name of the character.
     */
    public String getName() {
        return name;
    }

    /**
     * Get the level of the character
     * @return The level of the character
     */
    public int getLevel() {
        return level;
    }

    /**
     * Calculate the damage per second the character can do.
     * @return The damage per second the character can do.
     */
    public double getDPS() {
        return ((Weapon)equipment.get(Slot.WEAPON)).getDPS();
    }

    /**
     * Get an overview of all the info of the current character.
     * @return An overview of all the info of the current character.
     */
    public String getCharacterInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("Name: "); sb.append(this.name);
        sb.append("\nLevel: "); sb.append(this.level);
        sb.append("\nStrength: "); sb.append(this.getTotalPrimaryAttributes().getStrength());
        sb.append("\nDexterity: "); sb.append(this.getTotalPrimaryAttributes().getDexterity());
        sb.append("\nIntelligence: "); sb.append(this.getTotalPrimaryAttributes().getIntelligence());
        sb.append("\nDPS: "); sb.append(this.getDPS());
        return sb.toString();
    }
}

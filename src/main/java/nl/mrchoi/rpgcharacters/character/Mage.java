package nl.mrchoi.rpgcharacters.character;

import nl.mrchoi.rpgcharacters.exception.InvalidArmorException;
import nl.mrchoi.rpgcharacters.exception.InvalidWeaponException;
import nl.mrchoi.rpgcharacters.item.Armor;
import nl.mrchoi.rpgcharacters.item.Weapon;
import nl.mrchoi.rpgcharacters.util.ArmorType;
import nl.mrchoi.rpgcharacters.util.MathUtils;
import nl.mrchoi.rpgcharacters.util.PrimaryAttributes;
import nl.mrchoi.rpgcharacters.util.WeaponType;

import java.util.Arrays;
import java.util.HashSet;

/**
 * The mage character.
 */
public class Mage extends Character {
    private static final HashSet<WeaponType> ALLOWED_WEAPON_TYPES =
            new HashSet<>(Arrays.asList(WeaponType.WAND, WeaponType.STAFF));
    private static final HashSet<ArmorType> ALLOWED_ARMOR_TYPES =
            new HashSet<>(Arrays.asList(ArmorType.CLOTH));
    /**
     * Initialize new mage.
     *
     * @param name  The name of the mage.
     * @param level The level the mage starts with.
     */
    public Mage(String name, int level) {
        super(name, level, new PrimaryAttributes(1,1,8),
                new PrimaryAttributes(1,1,5));
    }

    public Mage(String name) {
        this(name, 1);
    }

    /**
     * Equip a weapon for the current mage. If it already has a weapon equipped, it will be replaced.
     * @param weapon The weapon to be equipped.
     */
    @Override
    public void equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if (!ALLOWED_WEAPON_TYPES.contains(weapon.getType()))
            throw new InvalidWeaponException(this, weapon.getType());
        super.equipWeapon(weapon);
    }

    /**
     * Equip armor for the current mage. If it already has armor equipped at a specific slot, it will be replaced.
     * @param armor The armor to be equipped.
     */
    @Override
    public void equipArmor(Armor armor) throws InvalidArmorException {
        if (!ALLOWED_ARMOR_TYPES.contains(armor.getType()))
            throw new InvalidArmorException(this, armor.getType());
        super.equipArmor(armor);
    }

    /**
     * Calculate the damage per second the mage can do.
     * @return The damage per second the mage can do.
     */
    @Override
    public double getDPS() {
        return super.getDPS() * MathUtils.increaseFactor(this.getTotalPrimaryAttributes().getIntelligence());
    }
}

package nl.mrchoi.rpgcharacters.util;

/**
 * Special class with frequently used calculation methods.
 */
public final class MathUtils {
    /**
     * Convert a percentual gain into a multiplication factor.
     * @param percentage The percentage.
     * @return The resulting.
     */
    public static double increaseFactor(double percentage) {
        return (1 + percentage/100.0);
    }
}

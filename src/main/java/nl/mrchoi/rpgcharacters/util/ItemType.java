package nl.mrchoi.rpgcharacters.util;

/**
 * Each kind of item has an implementation for types.
 */
public interface ItemType {
    public String getTypeName();
}

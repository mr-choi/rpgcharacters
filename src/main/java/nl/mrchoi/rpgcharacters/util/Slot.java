package nl.mrchoi.rpgcharacters.util;

/**
 * Possible slots where items can be equipped.
 */
public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}

package nl.mrchoi.rpgcharacters.util;

/**
 * Types of armor.
 */
public enum ArmorType implements ItemType {
    CLOTH("Cloth"),
    LEATHER("Leather"),
    MAIL("Mail"),
    PLATE("Plate");

    private String type;

    ArmorType(String type) {
        this.type = type;
    }

    @Override
    public String getTypeName() {
        return this.type;
    }
}

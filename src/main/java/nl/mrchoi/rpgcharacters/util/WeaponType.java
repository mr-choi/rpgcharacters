package nl.mrchoi.rpgcharacters.util;

/**
 * Types of weapons.
 */
public enum WeaponType implements ItemType {
    AXE("Axe"),
    BOW("Bow"),
    DAGGER("Dagger"),
    HAMMER("Hammer"),
    STAFF("Staff"),
    SWORD("Sword"),
    WAND("Wand");

    private String type;

    WeaponType(String type) {
        this.type = type;
    }

    @Override
    public String getTypeName() {
        return this.type;
    }
}

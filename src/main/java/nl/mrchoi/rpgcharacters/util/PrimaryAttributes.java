package nl.mrchoi.rpgcharacters.util;

/**
 * Primary attributes (i.e. stats) that all characters have.
 */
public class PrimaryAttributes {
    private int strength;
    private int dexterity;
    private int intelligence;

    /**
     * Create new primary attributes instance.
     * @param strength determine the physical strength of the character.
     * @param dexterity determine the speed and nimbleness of the character.
     * @param intelligence determine the affinity with magic of a character.
     */
    public PrimaryAttributes(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    /**
     * Add another instance of attributes to the current primary attributes.
     * @param attributes the attributes to be added.
     * @return the resulting attributes.
     */
    public PrimaryAttributes add(PrimaryAttributes attributes) {
        return new PrimaryAttributes(this.strength + attributes.strength,
                this.dexterity + attributes.dexterity,
                this.intelligence + attributes.intelligence);
    }

    /**
     * Subtract another instance of attributes to the current primary attributes.
     * @param attributes the attributes to be subtracted.
     * @return The resulting attributes.
     */
    public PrimaryAttributes subtract(PrimaryAttributes attributes) {
        return new PrimaryAttributes(this.strength - attributes.strength,
                this.dexterity - attributes.dexterity,
                this.intelligence - attributes.intelligence);
    }

    /**
     * Multiply all attributes with a factor.
     * @param factor The multiplication factor.
     * @return The resulting attributes.
     */
    public PrimaryAttributes multiply(int factor) {
        return new PrimaryAttributes(factor * this.strength, factor * this.dexterity,
                factor * this.intelligence);
    }

    public boolean same(PrimaryAttributes other) {
        return this.intelligence == other.intelligence && this.dexterity == other.dexterity
                && this.strength == other.strength;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }
}

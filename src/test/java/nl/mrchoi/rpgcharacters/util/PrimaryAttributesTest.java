package nl.mrchoi.rpgcharacters.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PrimaryAttributesTest {
    @Test
    public void shouldAddCorrectly() {
        int strengthA = -2, dexterityA = 8, intelligenceA = 4;
        PrimaryAttributes a = new PrimaryAttributes(strengthA, dexterityA, intelligenceA);
        int strengthB = 11, dexterityB = 0, intelligenceB = -6;
        PrimaryAttributes b = new PrimaryAttributes(strengthB, dexterityB, intelligenceB);

        int expectedStrength = strengthA + strengthB;
        int expectedDexterity = dexterityA + dexterityB;
        int expectedIntelligence = intelligenceA + intelligenceB;
        PrimaryAttributes expected = new PrimaryAttributes(expectedStrength, expectedDexterity, expectedIntelligence);

        PrimaryAttributes actual = a.add(b);

        assertTrue(expected.same(actual));
    }

    @Test
    public void shouldSubtractCorrectly() {
        int strengthA = -2, dexterityA = 8, intelligenceA = 4;
        PrimaryAttributes a = new PrimaryAttributes(strengthA, dexterityA, intelligenceA);
        int strengthB = 11, dexterityB = 0, intelligenceB = -6;
        PrimaryAttributes b = new PrimaryAttributes(strengthB, dexterityB, intelligenceB);

        int expectedStrength = strengthA - strengthB;
        int expectedDexterity = dexterityA - dexterityB;
        int expectedIntelligence = intelligenceA - intelligenceB;
        PrimaryAttributes expected = new PrimaryAttributes(expectedStrength, expectedDexterity, expectedIntelligence);

        PrimaryAttributes actual = a.subtract(b);

        assertTrue(expected.same(actual));
    }

    @Test
    public void shouldMultiplyCorrectlyWithPositiveFactor() {
        int strength = 3, dexterity = 0, intelligence = -2;
        int factor = 2;
        PrimaryAttributes x = new PrimaryAttributes(strength, dexterity, intelligence);

        int expectedStrength = strength * factor;
        int expectedDexterity = dexterity * factor;
        int expectedIntelligence = intelligence * factor;
        PrimaryAttributes expected = new PrimaryAttributes(expectedStrength, expectedDexterity, expectedIntelligence);

        PrimaryAttributes actual = x.multiply(factor);

        assertTrue(expected.same(actual));
    }

    @Test
    public void shouldMultiplyCorrectlyWithNegativeFactor() {
        int strength = 3, dexterity = 0, intelligence = -2;
        int factor = -1;
        PrimaryAttributes x = new PrimaryAttributes(strength, dexterity, intelligence);

        int expectedStrength = strength * factor;
        int expectedDexterity = dexterity * factor;
        int expectedIntelligence = intelligence * factor;
        PrimaryAttributes expected = new PrimaryAttributes(expectedStrength, expectedDexterity, expectedIntelligence);

        PrimaryAttributes actual = x.multiply(factor);

        assertTrue(expected.same(actual));
    }

    @Test
    public void shouldMultiplyCorrectlyWithFactorZero() {
        int strength = 3, dexterity = 0, intelligence = -2;
        int factor = 0;
        PrimaryAttributes x = new PrimaryAttributes(strength, dexterity, intelligence);

        int expectedStrength = strength * factor;
        int expectedDexterity = dexterity * factor;
        int expectedIntelligence = intelligence * factor;
        PrimaryAttributes expected = new PrimaryAttributes(expectedStrength, expectedDexterity, expectedIntelligence);

        PrimaryAttributes actual = x.multiply(factor);

        assertTrue(expected.same(actual));
    }
}
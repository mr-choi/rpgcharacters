package nl.mrchoi.rpgcharacters.character;

import nl.mrchoi.rpgcharacters.exception.InvalidArmorException;
import nl.mrchoi.rpgcharacters.exception.InvalidWeaponException;
import nl.mrchoi.rpgcharacters.item.Armor;
import nl.mrchoi.rpgcharacters.item.Weapon;
import nl.mrchoi.rpgcharacters.util.ArmorType;
import nl.mrchoi.rpgcharacters.util.PrimaryAttributes;
import nl.mrchoi.rpgcharacters.util.Slot;
import nl.mrchoi.rpgcharacters.util.WeaponType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {
    @Test
    public void shouldInitializeWithCorrectPrimaryAttributes() {
        String name = "Valla";
        Ranger ranger = new Ranger(name);

        int expectedStrength = 1;
        int expectedDexterity = 7;
        int expectedIntelligence = 1;
        PrimaryAttributes expectedAttributes =
                new PrimaryAttributes(expectedStrength, expectedDexterity, expectedIntelligence);

        PrimaryAttributes actualAttributes = ranger.getTotalPrimaryAttributes();

        assertTrue(expectedAttributes.same(actualAttributes));
    }
    @Test
    public void shouldLevelUpWithCorrectPrimaryAttributes() {
        String name = "Valla";
        Ranger ranger = new Ranger(name);
        ranger.levelUp();

        int expectedStrength = 1 + 1;
        int expectedDexterity = 7 + 5;
        int expectedIntelligence = 1 + 1;
        PrimaryAttributes expectedAttributes =
                new PrimaryAttributes(expectedStrength, expectedDexterity, expectedIntelligence);

        PrimaryAttributes actualAttributes = ranger.getTotalPrimaryAttributes();

        assertTrue(expectedAttributes.same(actualAttributes));
    }
    @Test
    public void shouldHaveCorrectDPSWhenEquippingWeapon() {
        String rangerName = "Valla";
        Ranger ranger = new Ranger(rangerName);
        String weaponName = "Short Bow";
        int minimumLevel = 1;
        WeaponType type = WeaponType.BOW;
        double damage = 4.5;
        double attackSpeed = 1.4;
        Weapon weapon = new Weapon(weaponName, minimumLevel, type, damage, attackSpeed);
        assertDoesNotThrow(() -> ranger.equipWeapon(weapon));

        double expectedDPS = damage * attackSpeed * (1 + 0.07);

        double actualDPS = ranger.getDPS();

        double delta = 1e-7;

        assertEquals(expectedDPS, actualDPS, delta);
    }
    @Test
    public void shouldThrowExceptionIfWeaponTypeCannotBeEquippedByRanger() {
        String rangerName = "Valla";
        Ranger ranger = new Ranger(rangerName);
        String weaponName = "Apprentice's Wand";
        int minimumLevel = 1;
        WeaponType type = WeaponType.WAND;
        double damage = 2.5;
        double attackSpeed = 1.2;
        Weapon weapon = new Weapon(weaponName, minimumLevel, type, damage, attackSpeed);

        String expectedMessage = "Valla cannot equip weapon of type Wand.";

        InvalidWeaponException e = assertThrows(InvalidWeaponException.class, () -> ranger.equipWeapon(weapon));
        String actualMessage = e.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }
    @Test
    public void shouldThrowExceptionIfArmorTypeCannotBeEquippedByRanger() {
        String rangerName = "Valla";
        Ranger ranger = new Ranger(rangerName);
        String armorName = "Chest plate";
        int minimumLevel = 1;
        ArmorType type = ArmorType.PLATE;
        Slot slot = Slot.BODY;

        int strength = 3;
        int dexterity = 0;
        int intelligence = 0;
        PrimaryAttributes attributes = new PrimaryAttributes(strength, dexterity, intelligence);

        Armor armor = new Armor(armorName, minimumLevel, slot, type, attributes);
        InvalidArmorException e = assertThrows(InvalidArmorException.class, () -> ranger.equipArmor(armor));

        String expectedMessage = "Valla cannot equip armor of type Plate.";

        String actualMessage = e.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }
}
package nl.mrchoi.rpgcharacters.character;

import nl.mrchoi.rpgcharacters.exception.InvalidArmorException;
import nl.mrchoi.rpgcharacters.exception.InvalidWeaponException;
import nl.mrchoi.rpgcharacters.item.Armor;
import nl.mrchoi.rpgcharacters.item.Weapon;
import nl.mrchoi.rpgcharacters.util.ArmorType;
import nl.mrchoi.rpgcharacters.util.PrimaryAttributes;
import nl.mrchoi.rpgcharacters.util.Slot;
import nl.mrchoi.rpgcharacters.util.WeaponType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {
    @Test
    public void shouldInitializeWithCorrectPrimaryAttributes() {
        String name = "Isendra";
        Mage mage = new Mage(name);

        int expectedStrength = 1;
        int expectedDexterity = 1;
        int expectedIntelligence = 8;
        PrimaryAttributes expectedAttributes =
                new PrimaryAttributes(expectedStrength, expectedDexterity, expectedIntelligence);

        PrimaryAttributes actualAttributes = mage.getTotalPrimaryAttributes();

        assertTrue(expectedAttributes.same(actualAttributes));
    }
    @Test
    public void shouldLevelUpWithCorrectPrimaryAttributes() {
        String name = "Isendra";
        Mage mage = new Mage(name);
        mage.levelUp();

        int expectedStrength = 1 + 1;
        int expectedDexterity = 1 + 1;
        int expectedIntelligence = 8 + 5;
        PrimaryAttributes expectedAttributes =
                new PrimaryAttributes(expectedStrength, expectedDexterity, expectedIntelligence);

        PrimaryAttributes actualAttributes = mage.getTotalPrimaryAttributes();

        assertTrue(expectedAttributes.same(actualAttributes));
    }
    @Test
    public void shouldHaveCorrectDPSWhenEquippingWeapon() {
        String mageName = "Isendra";
        Mage mage = new Mage(mageName);
        String weaponName = "Apprentice's wand";
        int minimumLevel = 1;
        WeaponType type = WeaponType.WAND;
        double damage = 2.5;
        double attackSpeed = 1.2;
        Weapon weapon = new Weapon(weaponName, minimumLevel, type, damage, attackSpeed);
        assertDoesNotThrow(() -> mage.equipWeapon(weapon));

        double expectedDPS = damage * attackSpeed * (1 + 0.08);

        double actualDPS = mage.getDPS();

        double delta = 1e-7;

        assertEquals(expectedDPS, actualDPS, delta);
    }
    @Test
    public void shouldThrowExceptionIfWeaponTypeCannotBeEquippedByMage() {
        String mageName = "Isendra";
        Mage mage = new Mage(mageName);
        String weaponName = "Short Bow";
        int minimumLevel = 1;
        WeaponType type = WeaponType.BOW;
        double damage = 4.5;
        double attackSpeed = 1.4;
        Weapon weapon = new Weapon(weaponName, minimumLevel, type, damage, attackSpeed);

        String expectedMessage = "Isendra cannot equip weapon of type Bow.";

        InvalidWeaponException e = assertThrows(InvalidWeaponException.class, () -> mage.equipWeapon(weapon));
        String actualMessage = e.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }
    @Test
    public void shouldThrowExceptionIfArmorTypeCannotBeEquippedByMage() {
        String mageName = "Isendra";
        Mage mage = new Mage(mageName);
        String armorName = "Chest plate";
        int minimumLevel = 1;
        ArmorType type = ArmorType.PLATE;
        Slot slot = Slot.BODY;

        int strength = 3;
        int dexterity = 0;
        int intelligence = 0;
        PrimaryAttributes attributes = new PrimaryAttributes(strength, dexterity, intelligence);

        Armor armor = new Armor(armorName, minimumLevel, slot, type, attributes);
        InvalidArmorException e = assertThrows(InvalidArmorException.class, () -> mage.equipArmor(armor));

        String expectedMessage = "Isendra cannot equip armor of type Plate.";

        String actualMessage = e.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }
}

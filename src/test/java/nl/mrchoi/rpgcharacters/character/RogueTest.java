package nl.mrchoi.rpgcharacters.character;

import nl.mrchoi.rpgcharacters.exception.InvalidArmorException;
import nl.mrchoi.rpgcharacters.exception.InvalidWeaponException;
import nl.mrchoi.rpgcharacters.item.Armor;
import nl.mrchoi.rpgcharacters.item.Weapon;
import nl.mrchoi.rpgcharacters.util.ArmorType;
import nl.mrchoi.rpgcharacters.util.PrimaryAttributes;
import nl.mrchoi.rpgcharacters.util.Slot;
import nl.mrchoi.rpgcharacters.util.WeaponType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RogueTest {
    @Test
    public void shouldInitializeWithCorrectPrimaryAttributes() {
        String name = "Moreina";
        Rogue rogue = new Rogue(name);

        int expectedStrength = 2;
        int expectedDexterity = 6;
        int expectedIntelligence = 1;
        PrimaryAttributes expectedAttributes =
                new PrimaryAttributes(expectedStrength, expectedDexterity, expectedIntelligence);

        PrimaryAttributes actualAttributes = rogue.getTotalPrimaryAttributes();

        assertTrue(expectedAttributes.same(actualAttributes));
    }
    @Test
    public void shouldLevelUpWithCorrectPrimaryAttributes() {
        String name = "Moreina";
        Rogue rogue = new Rogue(name);
        rogue.levelUp();

        int expectedStrength = 2 + 1;
        int expectedDexterity = 6 + 4;
        int expectedIntelligence = 1 + 1;
        PrimaryAttributes expectedAttributes =
                new PrimaryAttributes(expectedStrength, expectedDexterity, expectedIntelligence);

        PrimaryAttributes actualAttributes = rogue.getTotalPrimaryAttributes();

        assertTrue(expectedAttributes.same(actualAttributes));
    }
    @Test
    public void shouldHaveCorrectDPSWhenEquippingWeapon() {
        String rogueName = "Moreina";
        Rogue rogue = new Rogue(rogueName);
        String weaponName = "Small Dagger";
        int minimumLevel = 1;
        WeaponType type = WeaponType.DAGGER;
        double damage = 2.0;
        double attackSpeed = 1.8;
        Weapon weapon = new Weapon(weaponName, minimumLevel, type, damage, attackSpeed);
        assertDoesNotThrow(() -> rogue.equipWeapon(weapon));

        double expectedDPS = damage * attackSpeed * (1 + 0.06);

        double actualDPS = rogue.getDPS();

        double delta = 1e-7;

        assertEquals(expectedDPS, actualDPS, delta);
    }
    @Test
    public void shouldThrowExceptionIfWeaponTypeCannotBeEquippedByRogue() {
        String rogueName = "Moreina";
        Rogue rogue = new Rogue(rogueName);
        String weaponName = "Short Bow";
        int minimumLevel = 1;
        WeaponType type = WeaponType.BOW;
        double damage = 4.5;
        double attackSpeed = 1.4;
        Weapon weapon = new Weapon(weaponName, minimumLevel, type, damage, attackSpeed);

        String expectedMessage = "Moreina cannot equip weapon of type Bow.";

        InvalidWeaponException e = assertThrows(InvalidWeaponException.class, () -> rogue.equipWeapon(weapon));
        String actualMessage = e.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }
    @Test
    public void shouldThrowExceptionIfArmorTypeCannotBeEquippedByRogue() {
        String rogueName = "Moreina";
        Rogue rogue = new Rogue(rogueName);
        String armorName = "Sorcerer's Cloak";
        int minimumLevel = 1;
        ArmorType type = ArmorType.CLOTH;
        Slot slot = Slot.BODY;

        int strength = 0;
        int dexterity = 0;
        int intelligence = 5;
        PrimaryAttributes attributes = new PrimaryAttributes(strength, dexterity, intelligence);

        Armor armor = new Armor(armorName, minimumLevel, slot, type, attributes);
        InvalidArmorException e = assertThrows(InvalidArmorException.class, () -> rogue.equipArmor(armor));

        String expectedMessage = "Moreina cannot equip armor of type Cloth.";

        String actualMessage = e.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }
}
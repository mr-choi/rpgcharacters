package nl.mrchoi.rpgcharacters.character;

import nl.mrchoi.rpgcharacters.exception.InvalidArmorException;
import nl.mrchoi.rpgcharacters.exception.InvalidWeaponException;
import nl.mrchoi.rpgcharacters.item.Armor;
import nl.mrchoi.rpgcharacters.item.Weapon;
import nl.mrchoi.rpgcharacters.util.ArmorType;
import nl.mrchoi.rpgcharacters.util.PrimaryAttributes;
import nl.mrchoi.rpgcharacters.util.Slot;
import nl.mrchoi.rpgcharacters.util.WeaponType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {
    @Test
    public void shouldUpdateLevelWhenLevelUp() {
        String name = "Isendra";
        Mage mage = new Mage(name);
        mage.levelUp();

        int expected = 1 + 1;
        int actual = mage.getLevel();

        assertEquals(expected, actual);
    }
    @Test
    public void shouldCalculatePrimaryAttributesCorrectlyWhenInitializedWithHigherLevel() {
        String name = "Isendra";
        int level = 10;
        Mage mage = new Mage(name, level);

        int expectedStrength = 1 + (level-1)*1;
        int expectedDexterity = 1 + (level-1)*1;
        int expectedIntelligence = 8 + (level-1)*5;
        PrimaryAttributes expectedAttributes =
                new PrimaryAttributes(expectedStrength, expectedDexterity, expectedIntelligence);

        PrimaryAttributes actualAttributes = mage.getTotalPrimaryAttributes();

        assertTrue(expectedAttributes.same(actualAttributes));
    }
    @Test
    public void shouldThrowExceptionIfLevelOfWeaponIsTooHigh() {
        String mageName = "Isendra";
        Mage mage = new Mage(mageName);
        String weaponName = "Guardian";
        int minimumLevel = 9;
        WeaponType type = WeaponType.WAND;
        double damage = 91.0;
        double attackSpeed = 1.4;
        Weapon weapon = new Weapon(weaponName, minimumLevel, type, damage, attackSpeed);

        String expectedMessage = "Required level to equip Guardian (9) is higher than level of Isendra (1).";

        InvalidWeaponException e = assertThrows(InvalidWeaponException.class, () -> mage.equipWeapon(weapon));
        String actualMessage = e.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }
    @Test
    public void shouldHaveCorrectPrimaryAttributesWhenEquippingArmor() {
        String mageName = "Isendra";
        Mage mage = new Mage(mageName);
        String armorName = "Apprentice's cloak";
        int minimumLevel = 1;
        ArmorType type = ArmorType.CLOTH;
        Slot slot = Slot.BODY;

        int strength = 0;
        int dexterity = 1;
        int intelligence = 2;
        PrimaryAttributes attributes = new PrimaryAttributes(strength, dexterity, intelligence);

        Armor armor = new Armor(armorName, minimumLevel, slot, type, attributes);
        assertDoesNotThrow(() -> mage.equipArmor(armor));

        int expectedStrength = strength + 1;
        int expectedDexterity = dexterity + 1;
        int expectedIntelligence = intelligence + 8;
        PrimaryAttributes expectedAttributes = new PrimaryAttributes(expectedStrength, expectedDexterity,
                expectedIntelligence);

        PrimaryAttributes actualAttributes = mage.getTotalPrimaryAttributes();

        assertTrue(expectedAttributes.same(actualAttributes));
    }
    @Test
    public void shouldHaveCorrectPrimaryAttributesWhenUnequippingArmor() {
        String mageName = "Isendra";
        Mage mage = new Mage(mageName);
        String armorName = "Apprentice's cloak";
        int minimumLevel = 1;
        ArmorType type = ArmorType.CLOTH;
        Slot slot = Slot.BODY;

        int strength = 0;
        int dexterity = 1;
        int intelligence = 2;
        PrimaryAttributes attributes = new PrimaryAttributes(strength, dexterity, intelligence);

        Armor armor = new Armor(armorName, minimumLevel, slot, type, attributes);
        assertDoesNotThrow(() -> mage.equipArmor(armor));
        mage.unequip(Slot.BODY);

        int expectedStrength = 1;
        int expectedDexterity = 1;
        int expectedIntelligence = 8;
        PrimaryAttributes expectedAttributes = new PrimaryAttributes(expectedStrength, expectedDexterity,
                expectedIntelligence);

        PrimaryAttributes actualAttributes = mage.getTotalPrimaryAttributes();

        assertTrue(expectedAttributes.same(actualAttributes));
    }
    @Test
    public void shouldThrowExceptionIfLevelOfArmorIsTooHigh() {
        String mageName = "Isendra";
        Mage mage = new Mage(mageName);
        String armorName = "Guardian boots";
        int minimumLevel = 2;
        ArmorType type = ArmorType.CLOTH;
        Slot slot = Slot.LEGS;

        int strength = 0;
        int dexterity = 1;
        int intelligence = 6;
        PrimaryAttributes attributes = new PrimaryAttributes(strength, dexterity, intelligence);

        Armor armor = new Armor(armorName, minimumLevel, slot, type, attributes);
        InvalidArmorException e = assertThrows(InvalidArmorException.class, () -> mage.equipArmor(armor));

        String expectedMessage = "Required level to equip Guardian boots (2) is higher than level of Isendra (1).";

        String actualMessage = e.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }
    @Test
    public void shouldReturnStringInfoCorrectly() {
        String name = "Hero";
        int level = 2;
        Rogue rogue = new Rogue(name, level);

        String weaponName = "Short sword";
        int minimumLevel = 2;
        WeaponType weaponType = WeaponType.SWORD;
        double damage = 10 / 1.4;
        double attackSpeed = 1.4;
        Weapon weapon = new Weapon(weaponName, minimumLevel, weaponType, damage, attackSpeed);

        String armorName = "Helmet";
        int minimumLevelArmor = 2;
        ArmorType armorType = ArmorType.MAIL;
        Slot slot = Slot.HEAD;
        int strength = 0;
        int dexterity = 10;
        int intelligence = 0;
        PrimaryAttributes attributes = new PrimaryAttributes(strength, dexterity, intelligence);
        Armor armor = new Armor(armorName, minimumLevelArmor, slot, armorType, attributes);

        assertDoesNotThrow(() -> rogue.equipWeapon(weapon));
        assertDoesNotThrow(() -> rogue.equipArmor(armor));

        String expectedMessage = """
                Name: Hero
                Level: 2
                Strength: 3
                Dexterity: 20
                Intelligence: 2
                DPS: 12.0""";
        String actualMessage = rogue.getCharacterInfo();

        assertEquals(expectedMessage, actualMessage);
    }
}
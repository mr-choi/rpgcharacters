package nl.mrchoi.rpgcharacters.character;

import nl.mrchoi.rpgcharacters.exception.InvalidArmorException;
import nl.mrchoi.rpgcharacters.exception.InvalidWeaponException;
import nl.mrchoi.rpgcharacters.item.Armor;
import nl.mrchoi.rpgcharacters.item.Weapon;
import nl.mrchoi.rpgcharacters.util.ArmorType;
import nl.mrchoi.rpgcharacters.util.PrimaryAttributes;
import nl.mrchoi.rpgcharacters.util.Slot;
import nl.mrchoi.rpgcharacters.util.WeaponType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {
    @Test
    public void shouldInitializeWithCorrectPrimaryAttributes() {
        String name = "Aidan";
        Warrior warrior = new Warrior(name);

        int expectedStrength = 5;
        int expectedDexterity = 2;
        int expectedIntelligence = 1;
        PrimaryAttributes expectedAttributes =
                new PrimaryAttributes(expectedStrength, expectedDexterity, expectedIntelligence);

        PrimaryAttributes actualAttributes = warrior.getTotalPrimaryAttributes();

        assertTrue(expectedAttributes.same(actualAttributes));
    }
    @Test
    public void shouldLevelUpWithCorrectPrimaryAttributes() {
        String name = "Aidan";
        Warrior warrior = new Warrior(name);
        warrior.levelUp();

        int expectedStrength = 5 + 3;
        int expectedDexterity = 2 + 2;
        int expectedIntelligence = 1 + 1;
        PrimaryAttributes expectedAttributes =
                new PrimaryAttributes(expectedStrength, expectedDexterity, expectedIntelligence);

        PrimaryAttributes actualAttributes = warrior.getTotalPrimaryAttributes();

        assertTrue(expectedAttributes.same(actualAttributes));
    }
    @Test
    public void shouldHaveCorrectDPSWhenEquippingWeapon() {
        String warriorName = "Aidan";
        Warrior warrior = new Warrior(warriorName);
        String weaponName = "Small Hammer";
        int minimumLevel = 1;
        WeaponType type = WeaponType.HAMMER;
        double damage = 4.0;
        double attackSpeed = 1.4;
        Weapon weapon = new Weapon(weaponName, minimumLevel, type, damage, attackSpeed);
        assertDoesNotThrow(() -> warrior.equipWeapon(weapon));

        double expectedDPS = damage * attackSpeed * (1 + 0.05);

        double actualDPS = warrior.getDPS();

        double delta = 1e-7;

        assertEquals(expectedDPS, actualDPS, delta);
    }
    @Test
    public void shouldThrowExceptionIfWeaponTypeCannotBeEquippedByWarrior() {
        String warriorName = "Aidan";
        Warrior warrior = new Warrior(warriorName);
        String weaponName = "Apprentice's staff";
        int minimumLevel = 1;
        WeaponType type = WeaponType.STAFF;
        double damage = 4.0;
        double attackSpeed = 1.4;
        Weapon weapon = new Weapon(weaponName, minimumLevel, type, damage, attackSpeed);

        String expectedMessage = "Aidan cannot equip weapon of type Staff.";

        InvalidWeaponException e = assertThrows(InvalidWeaponException.class, () -> warrior.equipWeapon(weapon));
        String actualMessage = e.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }
    @Test
    public void shouldThrowExceptionIfArmorTypeCannotBeEquippedByWarrior() {
        String warriorName = "Aidan";
        Warrior warrior = new Warrior(warriorName);
        String armorName = "Leather Boots";
        int minimumLevel = 1;
        ArmorType type = ArmorType.LEATHER;
        Slot slot = Slot.LEGS;

        int strength = 1;
        int dexterity = 4;
        int intelligence = 0;
        PrimaryAttributes attributes = new PrimaryAttributes(strength, dexterity, intelligence);

        Armor armor = new Armor(armorName, minimumLevel, slot, type, attributes);
        InvalidArmorException e = assertThrows(InvalidArmorException.class, () -> warrior.equipArmor(armor));

        String expectedMessage = "Aidan cannot equip armor of type Leather.";

        String actualMessage = e.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }
}